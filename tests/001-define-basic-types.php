<?php
declare(strict_types=1);
require __DIR__.'/../vendor/autoload.php';

$stringSchema = new Charm\Schema\Schema('string', [
    'type' => ['string'],
]);
