<?php
declare(strict_types=1);

namespace Charm\Schema;

use JsonSerializable;

/**
 * @property string $path
 */
class Schema implements JsonSerializable
{
    public const TYPE_NULL = 'null';
    public const TYPE_INTEGER = 'integer';
    public const TYPE_NUMBER = 'number';
    public const TYPE_BOOLEAN = 'boolean';
    public const TYPE_STRING = 'string';
    public const TYPE_ARRAY = 'array';
    public const TYPE_OBJECT = 'object';

    /**
     *  @psalm-type SchemaArray = array{
     *      type: string|array<string>,
     *      title?: string,
     *      description?: string,
     *      default?: mixed,
     *      examples?: list<mixed>,
     *      readOnly?: bool,
     *      writeOnly?: bool,
     *      enum?: list<mixed>,
     *      const?: mixed,
     *      minLength?: int,
     *      maxLength?: int,
     *      format?: string,
     *      pattern?: string,
     *      multipleOf?: float|int,
     *      minimum?: float|int,
     *      exclusiveMinimum?: float|int|bool,
     *      maximum?: float|int,
     *      exclusiveMaximum?: float|int|bool,
     *      properties?: array<string, Schema>,
     *      patternProperties?: array<string, Schema>,
     *      additionalProperties?: bool|Schema,
     *      required?: list<string>,
     *      propertyNames?: Schema,
     *      minProperties?: int,
     *      maxProperties?: int,
     *      dependencies?: array<string, array<string>|self>,
     *      items?: Schema|array<Schema>,
     *      additionalItems?: bool,
     *      contains?: Schema|array,
     *      minItems?: int,
     *      maxItems?: int,
     *      uniqueItems?: bool,
     *      allOf?: list<Schema>,
     *      anyOf?: list<Schema>,
     *      not?: Schema|array
     *  }
     */

    /**
     * A name for this schema, unrelated to schema validation.
     */
    protected string $_path;

    /**
     * @see https://json-schema.org/understanding-json-schema/basics.html#the-type-keyword
     *
     * @var null|'null'|'integer'|'number'|'boolean'|'string'|'array'|'object'|array<'null'|'integer'|'number'|'boolean'|'string'|'array'|'object'>
     */
    public mixed $type;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/generic.html#annotations
     */
    public ?string $title = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/generic.html#annotations
     */
    public ?string $description = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/generic.html#annotations
     */
    public mixed $default = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/generic.html#annotations
     *
     * @var array<mixed>|null
     */
    public ?array $examples = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/generic.html#annotations
     */
    public ?bool $readOnly = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/generic.html#annotations
     */
    public ?bool $writeOnly = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/generic.html#enumerated-values
     */
    public ?array $enum = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/generic.html#constant-values
     */
    public mixed $const = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/string.html#id5
     */
    public ?int $minLength = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/string.html#id5
     */
    public ?int $maxLength = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/string.html#format
     *
     * @var 'date-time'|'time'|'date'|'email'|'idn-email'|'hostname'|'idn-hostname'|'ipv4'|'ipv6'|'uri'|'uri-reference'|'iri'|'iri-reference'|'json-pointer'|'relative-json-pointer'|'regex'|string|null
     */
    public ?string $format = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/regular_expressions.html
     */
    public ?string $pattern = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/numeric.html#multiples
     *
     * @var int|float|null
     */
    public mixed $multipleOf = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/numeric.html#range
     *
     * @var int|float|null
     */
    public mixed $minimum = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/numeric.html#range
     *
     * @var int|float|bool|null
     */
    public mixed $exclusiveMinimum = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/numeric.html#range
     *
     * @var int|float|null
     */
    public mixed $maximum = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/numeric.html#range
     *
     * @var int|float|bool|null
     */
    public mixed $exclusiveMaximum = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/object.html#properties
     *
     * @var array<string, self>|null
     */
    public ?array $properties = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/object.html#pattern-properties
     *
     * @var array<string, Schema>
     */
    public ?array $patternProperties = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/object.html#additional-properties
     *
     * @var bool|self|null
     */
    public mixed $additionalProperties = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/object.html#required-properties
     *
     * @var array<string>|null
     */
    public ?array $required = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/object.html#property-names
     */
    public ?self $propertyNames = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/object.html#size
     */
    public ?int $minProperties = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/object.html#size
     */
    public ?int $maxProperties = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/conditionals.html#dependencies
     *
     * @var array<string, array<string>|self>|null
     */
    public ?array $dependencies = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/array.html#items
     * @see https://json-schema.org/understanding-json-schema/reference/array.html#tuple-validation
     *
     * @var self|array<self>|null
     */
    public mixed $items;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/array.html#addtional-items
     */
    public ?bool $additionalItems = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/array.html#contains
     */
    public ?self $contains = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/array.html#length
     */
    public ?int $minItems = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/array.html#length
     */
    public ?int $maxItems = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/array.html#uniqueness
     */
    public ?bool $uniqueItems = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/combining.html#allof
     *
     * @var array<self>|null
     */
    public ?array $allOf = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/combining.html#anyof
     *
     * @var array<self>|null
     */
    public ?array $anyOf = null;

    /**
     * @see https://json-schema.org/understanding-json-schema/reference/combining.html#not
     */
    public ?self $not = null;

    /**
     * Schema extensions.
     */
    protected array $_x = [];

    /**
     *  @var array{
     *      extraProperties?: list<string>
     *  }
     */
    protected array $_options = [];

    /**
     * @psalm-assert SchemaArray $schema.
     *
     * @param string $path   A name to be used as a component in the schema path
     * @param array  $schema an array describing the schema
     */
    public function __construct(string $path, array $schema, array $options = [])
    {
        $this->_path = $path;
        $this->_options = $options;
        $schemaPath = $path;

        if (!\array_key_exists('type', $schema)) {
            throw new Error("'$path' does not have a 'type' property");
        }

        foreach ($schema as $key => $value) {
            if (\is_callable($value)) {
                $value = \call_user_func($value);
            }
            switch ($key) {
                case 'title':
                case 'description':
                case '$id':
                case '$schema':
                case '$comment':
                    if (!\is_string($value)) {
                        throw new Error("Schema '$schemaPath/$key' must be a string value");
                    }
                    $this->$key = $value;
                    break;
                case 'default':
                    $this->default = $value;
                    break;
                case 'examples':
                    if (!\is_array($value)) {
                        throw new Error("Schema '$schemaPath/examples' must be an array");
                    }
                    if (array_keys($value) !== range(0, \count($value) - 1)) {
                        throw new Error("Schema '$schemaPath/examples' must be a non-associative array");
                    }
                    $this->examples = $value;
                    break;
                case 'readOnly':
                case 'writeOnly':
                    if (!\is_bool($value)) {
                        throw new Error("Schema '$schemaPath/$key' must be a boolean value");
                    }
                    $this->$key = $value;
                    break;
                case 'enum':
                    if (!\is_array($value)) {
                        throw new Error("Schema '$schemaPath/$key' must be an array");
                    }
                    $this->enum = $value;
                    break;
                case 'const':
                    $this->const = $value;
                    break;
                case 'type':
                    if (\is_array($value)) {
                        if (array_keys($value) !== range(0, \count($value) - 1)) {
                            throw new Error("Schema '$schemaPath/type' must be a string or a non-associative array");
                        }
                        foreach ($value as $typeIndex => $type) {
                            if (!\in_array($type, [self::TYPE_ARRAY, self::TYPE_BOOLEAN, self::TYPE_INTEGER, self::TYPE_NULL, self::TYPE_NUMBER, self::TYPE_OBJECT, self::TYPE_STRING])) {
                                throw new Error("Schema '$schemaPath/type[$typeIndex]' must be one of 'array', 'boolean', 'integer', 'null', 'number', 'object' or 'string'");
                            }
                        }
                        $this->type = $value;
                    } elseif (\in_array($value, [self::TYPE_ARRAY, self::TYPE_BOOLEAN, self::TYPE_INTEGER, self::TYPE_NULL, self::TYPE_NUMBER, self::TYPE_OBJECT, self::TYPE_STRING])) {
                        $this->type = $value;
                    } elseif (\array_key_exists('enum', $schema)) {
                        $this->type = null;
                    } else {
                        throw new Error("Schema type must be one of 'array', 'boolean', 'integer', 'null', 'number', 'object' or 'string' or an array with multiple of these. '".$value."' is invalid");
                    }
                    break;
                case 'minLength':
                    if (!\is_int($value)) {
                        throw new Error("Schema 'minLength' must be an integer");
                    }
                    $this->minLength = $value;
                    break;
                case 'maxLength':
                    if (!\is_int($value)) {
                        throw new Error("Schema 'minLength' must be an integer");
                    }
                    $this->maxLength = $value;
                    break;
                case 'format':
                    if (!\is_string($value)) {
                        throw new Error("Schema 'minLength' must be an integer");
                    }
                    $this->format = $value;
                    break;
                case 'pattern':
                    if (!\is_string($value)) {
                        throw new Error("Schema 'pattern' must be a string");
                    }
                    $this->pattern = $value;
                    break;
                case 'multipleOf':
                    if (!\is_int($value) && !\is_float($value)) {
                        throw new Error("Schema 'multipleOf' must be a number");
                    }
                    $this->multipleOf = $value;
                    break;
                case 'minimum':
                    if (!\is_int($value) && !\is_float($value)) {
                        throw new Error("Schema 'minimum' must be a number");
                    }
                    $this->minimum = $value;
                    break;
                case 'exclusiveMinimum':
                    if (!\is_int($value) && !\is_float($value) && !\is_bool($value)) {
                        throw new Error("Schema 'exclusiveMinimum' must be a number or a boolean value");
                    }
                    $this->exclusiveMinimum = $value;
                    break;
                case 'maximum':
                    if (!\is_int($value) && !\is_float($value)) {
                        throw new Error("Schema 'maximum' must be a number");
                    }
                    $this->minimum = $value;
                    break;
                case 'exclusiveMaximum':
                    if (!\is_int($value) && !\is_float($value) && !\is_bool($value)) {
                        throw new Error("Schema 'excludeMaximum' must be a number or a boolean value");
                    }
                    $this->exclusiveMinimum = $value;
                    break;
                case 'properties':
                    if (!\is_array($value)) {
                        throw new Error("Schema '$schemaPath/properties' must be an associative array");
                    }
                    $properties = [];
                    foreach ($value as $propertyName => $propertySchema) {
                        if (!\is_string($propertyName)) {
                            throw new Error("Schema 'properties' must be an associative array where the key is a string representing the property name");
                        }
                        if (\is_array($propertySchema)) {
                            $properties[$propertyName] = new self("$schemaPath/properties/$propertyName", $propertySchema);
                        } elseif (\is_object($propertySchema) && is_a($propertySchema, self::class)) {
                            $properties[$propertyName] = $propertySchema;
                        } else {
                            throw new Error("Schema '$schemaPath/properties/$propertyName' must contain array schema definitions or a Schema instance");
                        }
                    }
                    $this->properties = $properties;
                    break;
                case 'patternProperties':
                    if (!\is_array($value)) {
                        throw new Error("Schema '$schemaPath/patternProperties' must be an associative array");
                    }
                    $patternProperties = [];
                    foreach ($value as $propertyName => $propertySchema) {
                        if (!\is_string($propertyName)) {
                            throw new Error("Schema '$schemaPath/patternProperties/$propertyName' is an invalid key. Keys must be regex strings.");
                        }
                        if (\is_array($propertySchema)) {
                            $patternProperties[$propertyName] = new self("$schemaPath/patternProperties/$propertyName", $propertySchema);
                        } elseif (\is_object($propertySchema) && is_a($propertySchema, self::class)) {
                            $patternProperties[$propertyName] = $propertySchema;
                        } else {
                            throw new Error("Schema '$schemaPath/patternProperties/$propertyName' must contain array schema definitions or a Schema instance");
                        }
                    }
                    $this->patternProperties = $patternProperties;
                    break;
                case 'additionalProperties':
                    if (!\is_bool($value)) {
                        throw new Error("Schema '$schemaPath/additionalProperties' must be a boolean value");
                    }
                    $this->additionalProperties = $value;
                    break;
                case 'required':
                    if (!\is_array($value)) {
                        throw new Error("Schema '$schemaPath/required' must be an array");
                    }
                    if (array_keys($value) !== range(0, \count($value) - 1)) {
                        throw new Error("Schema '$schemaPath/required' is an associative array");
                    }
                    foreach ($value as $propertyName) {
                        if (!\is_string($propertyName)) {
                            throw new Error("Schema '$schemaPath/required' declares a property name that is not a string");
                        }
                    }
                    $this->required = $value;
                    break;
                case 'propertyNames':
                    if (\is_array($value)) {
                        $this->propertyNames = new self("$schemaPath/propertyNames", $value);
                    } elseif (\is_object($value) && is_a($value, self::class)) {
                        $this->propertyNames = $value;
                    } else {
                        throw new Error("Schema '$schemaPath/propertyNames' must contain array schema definitions or a Schema instance");
                    }
                    break;
                case 'minProperties':
                    if (!\is_int($value)) {
                        throw new Error("Schema '$schemaPath/minProperties' must be an integer value");
                    }
                    $this->minProperties = $value;
                    break;
                case 'maxProperties':
                    if (!\is_int($value)) {
                        throw new Error("Schema '$schemaPath/maxProperties' must be an integer value");
                    }
                    $this->maxProperties = $value;
                    break;
                case 'dependencies': // array<string, array<string>|self>|null
                    if (!\is_array($value)) {
                        throw new Error("Schema '$schemaPath/dependencies' must be an array");
                    }
                    $dependencies = [];
                    foreach ($value as $lhs => $rhs) {
                        if (!\is_string($lhs)) {
                            throw new Error("Schema '$schemaPath/dependencies' must be an associative array with string keys");
                        }
                        if (\is_array($rhs)) {
                            $dependencies[$lhs] = new self("$schemaPath/dependencies/$lhs", $rhs);
                        } elseif (\is_object($rhs) && is_a($rhs, self::class)) {
                            $dependencies[$lhs] = $rhs;
                        } else {
                            throw new Error("Schema '$schemaPath/dependencies/$lhs' must contain array schema definitions or a Schema instance");
                        }
                    }
                    $this->dependencies = $dependencies;
                    break;
                case 'items':
                    if (\is_object($value) && is_a($value, self::class)) {
                        $this->items = $value;
                    } elseif (\is_array($value)) {
                        if (array_keys($value) === range(0, \count($value) - 1)) {
                            $items = [];
                            foreach ($value as $itemKey => $itemSchema) {
                                if (\is_array($itemSchema)) {
                                    $items[] = new self("$schemaPath/items[$itemKey]", $itemSchema);
                                } elseif (\is_object($itemSchema) && is_a($itemSchema, self::class)) {
                                    $items[] = $itemSchema;
                                } else {
                                    throw new Error("Schema '$schemaPath/items[]' must only contain array schema definitions or a Schema instance");
                                }
                            }
                            $this->items = $items;
                        } else {
                            $this->items = new self("$schemaPath/items", $value);
                        }
                    } else {
                        throw new Error("Schema '$schemaPath/items' must be a schema or an array of schemas");
                    }
                    break;
                case 'additionalItems':
                    if (!\is_bool($value)) {
                        throw new Error("Schema '$schemaPath/additionalItems' must be a boolean value");
                    }
                    $this->additionalItems = $value;
                    break;
                case 'contains':
                    if (\is_array($value)) {
                        $this->contains = new self("$schemaPath/contains", $value);
                    } elseif (\is_object($value) && is_a($value, self::class)) {
                        $this->contains = $value;
                    } else {
                        throw new Error("Schema '$schemaPath/contains' must be a schema");
                    }
                    break;
                case 'minItems':
                    if (!\is_int($value)) {
                        throw new Error("Schema '$schemaPath/minItems' must be an integer value");
                    }
                    $this->minItems = $value;
                    break;
                case 'maxItems':
                    if (!\is_int($value)) {
                        throw new Error("Schema '$schemaPath/maxItems' must be an integer value");
                    }
                    $this->maxItems = $value;
                    break;
                case 'uniqueItems':
                    if (!\is_bool($value)) {
                        throw new Error("Schema '$schemaPath/uniqueItems' must be a boolean value");
                    }
                    $this->uniqueItems = $value;
                    break;
                case 'anyOf':
                case 'allOf':
                    if (!\is_array($value)) {
                        throw new Error("Schema '$schemaPath/$key' must be an array");
                    }
                    if (array_keys($value) !== range(0, \count($value) - 1)) {
                        throw new Error("Schema '$schemaPath/$key' must be a non-associative array");
                    }
                    $values = [];
                    foreach ($value as $subSchemaIndex => $subSchema) {
                        if (\is_array($subSchema)) {
                            $values[] = new self("$schemaPath/".$key.'['.$subSchemaIndex.']', $subSchema);
                        } elseif (\is_object($subSchema) && is_a($subSchema, self::class)) {
                            $values[] = $subSchema;
                        } else {
                            throw new Error("Schema '$schemaPath/$key' must be an array of schemas");
                        }
                    }
                    $this->$key = $values;
                    break;
                case 'not':
                    if (\is_array($value)) {
                        $this->not = new self("$schemaPath/not", $value);
                    } elseif (\is_object($value) && is_a($value, self::class)) {
                        $this->not = $value;
                    } else {
                        throw new Error("Schema '$schemaPath/not' must be a schema");
                    }
                    break;

                default:
                    $this->__set($key, $value);
                    break;
            }
        }
    }

    public function __set(string $key, mixed $value)
    {
        if (0 === stripos($key, 'x-')) {
            $this->_x[$key] = $value;
        } elseif (isset($this->_options['extraProperties']) && \in_array($key, $this->_options['extraProperties'])) {
            $this->_x[$key] = $value;
        } else {
            throw new Error("Schema '".$this->_path."/$key' is an unrecognized property");
        }
    }

    public function __get(string $key)
    {
        switch ($key) {
            case 'path': return $this->_path;
            default:
                return $this->_x[$key] ?? null;
        }
    }

    public function __isset(string $key)
    {
        return \array_key_exists($key, $this->_x);
    }

    public function __unset(string $key)
    {
        unset($this->_x[$key]);
    }

    public function jsonSerialize()
    {
        $values = get_object_vars($this);

        $values += $this->_x;
        foreach ($values as $name => $value) {
            if (null === $value) {
                unset($values[$name]);
            } elseif ('_' === substr($name, 0, 1)) {
                unset($values[$name]);
            }
        }

        return $values;
    }
}
